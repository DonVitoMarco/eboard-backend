package pl.thewalkingcode.eboard.service.utils;

import pl.thewalkingcode.eboard.model.Board;
import pl.thewalkingcode.eboard.model.Note;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.model.request.BoardItem;
import pl.thewalkingcode.eboard.model.request.CreateRoomRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class TestUtils {

    public static Room createExampleRoomModel() {
        Room room = new Room();
        room.setUuid(UUID.randomUUID().toString());
        room.setTitle("TEST EBOARD");
        room.setCreateDate(new Date());

        for (int i = 0; i < 5; i++) {
            Board board = new Board();
            board.setUuid(UUID.randomUUID().toString());
            board.setName("BOARD" + i);
            for (int j = 0; j < 4; j++) {
                Note note = new Note();
                note.setUuid(UUID.randomUUID().toString());
                note.setColor("#ffffff");
                note.setDate(new Date().toString());
                note.setAuthor("Test user");
                note.setContent("Test Content");
                note.setTitle("Test title");
                note.setVotes(new ArrayList<String>() {{
                    add(UUID.randomUUID().toString());
                    add(UUID.randomUUID().toString());
                }});
                board.getNotes().add(note);
            }
            room.getBoards().add(board);
        }
        return room;
    }

    public static CreateRoomRequest createExampleCreateRoomRequestModel() {
        CreateRoomRequest createRoomRequest = new CreateRoomRequest();
        createRoomRequest.setTitle("NEW ROOM");
        for (int i = 0; i < 4; i++) {
            BoardItem boardItem = new BoardItem();
            boardItem.setName("NAME " + i);
            createRoomRequest.getBoardsList().add(boardItem);
        }
        return createRoomRequest;
    }

}
