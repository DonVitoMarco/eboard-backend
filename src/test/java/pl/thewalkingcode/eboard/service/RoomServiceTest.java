package pl.thewalkingcode.eboard.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.model.RoomUsers;
import pl.thewalkingcode.eboard.model.request.CreateRoomRequest;
import pl.thewalkingcode.eboard.repository.RoomRepository;
import pl.thewalkingcode.eboard.repository.RoomUsersRepository;
import pl.thewalkingcode.eboard.service.utils.TestUtils;

import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RoomServiceTest {

    @InjectMocks
    private RoomService roomService;

    @Mock
    private RoomRepository roomRepository;
    @Mock
    private RoomUsersRepository roomUsersRepository;

    @Test
    public void createRoom() {
        // given
        CreateRoomRequest createRoomRequest = TestUtils.createExampleCreateRoomRequestModel();
        when(roomRepository.save(any(Room.class))).thenReturn(null);
        when(roomUsersRepository.save(any(RoomUsers.class))).thenReturn(null);
        // when
        Room room = roomService.createRoom(createRoomRequest);
        // then
        verify(roomRepository, times(1)).save(any(Room.class));
        verify(roomUsersRepository, times(1)).save(any(RoomUsers.class));
        Assert.assertNotNull(room.getCreateDate());
        Assert.assertNotNull(room.getTitle());
        Assert.assertNotNull(room.getUsersUuid());
        Assert.assertNotNull(room.getBoards());
        Assert.assertEquals(createRoomRequest.getBoardsList().size(), room.getBoards().size());
    }

    @Test
    public void findRoom() {
        // given
        when(roomRepository.findOne(anyString())).thenReturn(null);
        String uuid = UUID.randomUUID().toString();
        // when
        roomService.findRoom(uuid);
        // then
        verify(roomRepository, times(1)).findOne(anyString());
    }

}
