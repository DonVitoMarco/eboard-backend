package pl.thewalkingcode.eboard.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.thewalkingcode.eboard.model.Board;
import pl.thewalkingcode.eboard.model.Note;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.repository.RoomRepository;
import pl.thewalkingcode.eboard.service.utils.TestUtils;
import pl.thewalkingcode.eboard.stomp.WebSocketMessaging;

import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class NoteServiceTest {

    @InjectMocks
    private NoteService noteService;

    @Mock
    RoomRepository roomRepository;
    @Mock
    WebSocketMessaging webSocketMessaging;

    @Test
    public void addNoteEvent() {
        // given
        Room room = TestUtils.createExampleRoomModel();
        Note note = new Note();
        note.setUuid(UUID.randomUUID().toString());
        String boardUuid = room.getBoards().get(1).getUuid();
        int expectedValue = room.getBoards().get(1).getNotes().size() + 1;
        // when
        noteService.addNoteEvent(room, boardUuid, note);
        // then
        Assert.assertEquals(expectedValue, room.getBoards().get(1).getNotes().size());
    }

    @Test
    public void removeNoteEvent() {
        // given
        Room room = TestUtils.createExampleRoomModel();
        Note note = room.getBoards().get(1).getNotes().get(0);
        int expectedValue = room.getBoards().get(1).getNotes().size() - 1;
        // when
        noteService.removeNoteEvent(room, note.getUuid());
        // then
        Assert.assertEquals(expectedValue, room.getBoards().get(1).getNotes().size());
    }

    @Test
    public void addVoteNoteEvent() {
        // given
        Room room = TestUtils.createExampleRoomModel();
        Note note = room.getBoards().get(1).getNotes().get(0);
        String uuidUser = UUID.randomUUID().toString();
        int expectedValue = room.getBoards().get(1).getNotes().get(0).getVotes().size() + 1;
        // when
        noteService.addVoteNoteEvent(room, note.getUuid(), uuidUser);
        // then
        Assert.assertEquals(expectedValue, room.getBoards().get(1).getNotes().get(0).getVotes().size());
    }

    @Test
    public void removeVoteNoteEvent() {
        // given
        Room room = TestUtils.createExampleRoomModel();
        Note note = room.getBoards().get(1).getNotes().get(0);
        String userUuid = note.getVotes().get(0);
        int expectedValue = room.getBoards().get(1).getNotes().get(0).getVotes().size() - 1;
        // when
        noteService.removeVoteNoteEvent(room, note.getUuid(), userUuid);
        // then
        Assert.assertEquals(expectedValue, room.getBoards().get(1).getNotes().get(0).getVotes().size());
    }

    @Test
    public void moveNoteEvent() {
        // given
        Room room = TestUtils.createExampleRoomModel();
        String boardUuid = room.getBoards().get(1).getUuid();
        Note note = room.getBoards().get(0).getNotes().get(0);
        int firstBoardExpectedValue = room.getBoards().get(0).getNotes().size() - 1;
        int secondBoardExpectedValue = room.getBoards().get(1).getNotes().size() + 1;
        // when
        noteService.moveNoteEvent(room, boardUuid, note);
        // then
        Assert.assertEquals(firstBoardExpectedValue, room.getBoards().get(0).getNotes().size());
        Assert.assertEquals(secondBoardExpectedValue, room.getBoards().get(1).getNotes().size());
    }

    @Test
    public void editNoteEvent() {
        // given
        Room room = TestUtils.createExampleRoomModel();
        String boardUuid = room.getBoards().get(0).getUuid();
        Note note = room.getBoards().get(0).getNotes().get(0);
        String expectedContent = "New Content";
        note.setContent(expectedContent);
        // when
        noteService.editNoteEvent(room, boardUuid, note);
        // then
        for (Board b : room.getBoards()) {
            for (Note n : b.getNotes()) {
                if (n.getUuid().equals(note.getUuid())) {
                    Assert.assertEquals(expectedContent, n.getContent());
                }
            }
        }
    }

}
