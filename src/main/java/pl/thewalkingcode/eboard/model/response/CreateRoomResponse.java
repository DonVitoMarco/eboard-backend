package pl.thewalkingcode.eboard.model.response;

public class CreateRoomResponse {

    private String uuid;

    public CreateRoomResponse(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

}
