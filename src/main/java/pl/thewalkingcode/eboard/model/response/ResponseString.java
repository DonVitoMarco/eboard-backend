package pl.thewalkingcode.eboard.model.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResponseString {

    private String responseString;

    public ResponseString(String responseString) {
        this.responseString = responseString;
    }

}
