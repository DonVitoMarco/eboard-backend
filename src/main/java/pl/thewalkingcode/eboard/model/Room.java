package pl.thewalkingcode.eboard.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
@Getter
@Setter
@NoArgsConstructor
public class Room {

    @Id
    private String uuid;

    private String usersUuid;
    private String title;
    private List<Board> boards = new ArrayList<>();

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createDate;

    public Room(String uuid, String title, String usersUuid, Date createDate) {
        this.uuid = uuid;
        this.title = title;
        this.usersUuid = usersUuid;
        this.createDate = createDate;
    }

}
