package pl.thewalkingcode.eboard.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Note {

    private String uuid;
    private String title;
    private String content;
    private String author;
    private String date;
    private List<String> votes = new ArrayList<>();
    private String color;

}
