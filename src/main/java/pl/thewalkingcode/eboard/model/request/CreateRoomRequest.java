package pl.thewalkingcode.eboard.model.request;

import java.util.ArrayList;
import java.util.List;

public class CreateRoomRequest {

    private String title;
    private List<BoardItem> boardsList = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<BoardItem> getBoardsList() {
        return boardsList;
    }

    public void setBoardsList(List<BoardItem> boardsList) {
        this.boardsList = boardsList;
    }

}
