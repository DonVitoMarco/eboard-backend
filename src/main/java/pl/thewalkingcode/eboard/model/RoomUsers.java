package pl.thewalkingcode.eboard.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
@Getter
@Setter
@NoArgsConstructor
public class RoomUsers {

    @Id
    private String uuid;

    private String roomUuid;
    private List<User> users = new ArrayList<>();

    public RoomUsers(String uuid, String roomUuid) {
        this.uuid = uuid;
        this.roomUuid = roomUuid;
    }

}
