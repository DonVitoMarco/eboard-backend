package pl.thewalkingcode.eboard.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class User {

    private String uuid;
    private String username;

    public User(String uuid, String username) {
        this.uuid = uuid;
        this.username = username;
    }

}
