package pl.thewalkingcode.eboard.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Board {

    private String uuid;

    private String name;
    private List<Note> notes = new ArrayList<>();

    public Board(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

}
