package pl.thewalkingcode.eboard.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.thewalkingcode.eboard.model.Room;

public interface RoomRepository extends MongoRepository<Room, String> {
}
