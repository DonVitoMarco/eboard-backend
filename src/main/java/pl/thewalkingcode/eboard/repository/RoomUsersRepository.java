package pl.thewalkingcode.eboard.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.thewalkingcode.eboard.model.RoomUsers;

public interface RoomUsersRepository extends MongoRepository<RoomUsers, String> {

    RoomUsers findByRoomUuid(String roomUuid);

}
