package pl.thewalkingcode.eboard.stomp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.thewalkingcode.eboard.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserMessage {

    private String action;
    private User user;

    public UserMessage(String action, User user) {
        this.action = action;
        this.user = user;
    }

}
