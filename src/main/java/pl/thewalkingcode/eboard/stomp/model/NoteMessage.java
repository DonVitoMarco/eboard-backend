package pl.thewalkingcode.eboard.stomp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.thewalkingcode.eboard.model.Note;

@Getter
@Setter
@NoArgsConstructor
public class NoteMessage {

    private String action;
    private Note note;
    private String targetUuid;
    private String sourceUuid;
    private String userUuid;

}
