package pl.thewalkingcode.eboard.stomp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class WebSocketMessaging {

    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public WebSocketMessaging(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public void sendToAllRoom(String roomUuid, String message) {
        this.simpMessagingTemplate.convertAndSend("/topic/room/" + roomUuid, message);
    }

    public void sendToAllUsers(String roomUuid, String message) {
        this.simpMessagingTemplate.convertAndSend("/topic/users/" + roomUuid, message);
    }

}
