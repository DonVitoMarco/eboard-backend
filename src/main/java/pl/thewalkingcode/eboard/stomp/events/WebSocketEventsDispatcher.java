package pl.thewalkingcode.eboard.stomp.events;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.repository.RoomRepository;
import pl.thewalkingcode.eboard.service.NoteService;
import pl.thewalkingcode.eboard.stomp.WebSocketMessaging;
import pl.thewalkingcode.eboard.stomp.model.NoteMessage;

@Service
public class WebSocketEventsDispatcher {

    private RoomRepository roomRepository;
    private WebSocketMessaging webSocketMessaging;
    private NoteService noteService;

    @Autowired
    public WebSocketEventsDispatcher(RoomRepository roomRepository, WebSocketMessaging webSocketMessaging, NoteService noteService) {
        this.roomRepository = roomRepository;
        this.webSocketMessaging = webSocketMessaging;
        this.noteService = noteService;
    }

    public void messageDispatcher(NoteMessage message, String roomUuid) {
        Room room = roomRepository.findOne(roomUuid);
        if (room == null) {
            return;
        }

        switch (message.getAction()) {
            case WebSocketEvents.ADD_NOTE:
                noteService.addNoteEvent(room, message.getTargetUuid(), message.getNote());
                saveAndSendToAll(room, message);
                break;
            case WebSocketEvents.REMOVE_NOTE:
                noteService.removeNoteEvent(room, message.getNote().getUuid());
                saveAndSendToAll(room, message);
                break;
            case WebSocketEvents.ADD_VOTE:
                noteService.addVoteNoteEvent(room, message.getNote().getUuid(), message.getUserUuid());
                saveAndSendToAll(room, message);
                break;
            case WebSocketEvents.REMOVE_VOTE:
                noteService.removeVoteNoteEvent(room, message.getNote().getUuid(), message.getUserUuid());
                saveAndSendToAll(room, message);
                break;
            case WebSocketEvents.MOVE_NOTE:
                noteService.moveNoteEvent(room, message.getTargetUuid(), message.getNote());
                saveAndSendToAll(room, message);
                break;
            case WebSocketEvents.EDIT_NOTE:
                noteService.editNoteEvent(room, message.getTargetUuid(), message.getNote());
                saveAndSendToAll(room, message);
                break;
            default:
                break;
        }
    }

    private void saveAndSendToAll(Room room, NoteMessage message) {
        roomRepository.save(room);
        Gson gson = new Gson();
        webSocketMessaging.sendToAllRoom(room.getUuid(), gson.toJson(message));
    }

}
