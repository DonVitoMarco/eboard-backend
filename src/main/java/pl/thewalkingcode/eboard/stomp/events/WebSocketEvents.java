package pl.thewalkingcode.eboard.stomp.events;

public class WebSocketEvents {

    private WebSocketEvents() {
    }

    public static final String ADD_NOTE = "ADD";
    public static final String MOVE_NOTE = "MOVE";
    public static final String REMOVE_NOTE = "REMOVE";
    public static final String EDIT_NOTE = "EDIT";
    public static final String ADD_VOTE = "ADD_VOTE";
    public static final String REMOVE_VOTE = "REMOVE_VOTE";

}
