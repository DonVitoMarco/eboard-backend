package pl.thewalkingcode.eboard.stomp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import pl.thewalkingcode.eboard.stomp.events.WebSocketEventsDispatcher;
import pl.thewalkingcode.eboard.stomp.model.NoteMessage;

@Controller
public class WebSocketController {

    private WebSocketEventsDispatcher webSocketEventsDispatcher;

    @Autowired
    public WebSocketController(WebSocketEventsDispatcher webSocketEventsDispatcher) {
        this.webSocketEventsDispatcher = webSocketEventsDispatcher;
    }

    @MessageMapping("/room/{roomUuid}")
    public void subscribeRoom(@DestinationVariable String roomUuid, @Payload NoteMessage message) {
        webSocketEventsDispatcher.messageDispatcher(message, roomUuid);
    }

}
