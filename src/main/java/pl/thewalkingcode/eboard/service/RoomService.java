package pl.thewalkingcode.eboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.thewalkingcode.eboard.model.Board;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.model.RoomUsers;
import pl.thewalkingcode.eboard.model.request.BoardItem;
import pl.thewalkingcode.eboard.model.request.CreateRoomRequest;
import pl.thewalkingcode.eboard.repository.RoomRepository;
import pl.thewalkingcode.eboard.repository.RoomUsersRepository;

import java.util.Date;
import java.util.UUID;

@Service
public class RoomService {

    private RoomRepository roomRepository;
    private RoomUsersRepository roomUsersRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository, RoomUsersRepository roomUsersRepository) {
        this.roomRepository = roomRepository;
        this.roomUsersRepository = roomUsersRepository;
    }

    public Room createRoom(CreateRoomRequest createRoomRequest) {
        String roomUsersUuid = UUID.randomUUID().toString();
        Room room = new Room(UUID.randomUUID().toString(), createRoomRequest.getTitle(), roomUsersUuid, new Date());
        for (BoardItem boardItem : createRoomRequest.getBoardsList()) {
            room.getBoards().add(new Board(UUID.randomUUID().toString(), boardItem.getName()));
        }
        roomRepository.save(room);
        RoomUsers roomUsers = new RoomUsers(roomUsersUuid, room.getUuid());
        roomUsersRepository.save(roomUsers);
        return room;
    }

    public Room findRoom(String roomUuid) {
        return roomRepository.findOne(roomUuid);
    }

}
