package pl.thewalkingcode.eboard.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.thewalkingcode.eboard.model.Note;
import pl.thewalkingcode.eboard.model.Room;

import java.util.stream.Collectors;

@Service
public class NoteService {

    @Transactional
    public void addNoteEvent(Room room, String boardUuid, Note note) {
        room.getBoards().forEach(board -> {
            if (board.getUuid().equals(boardUuid)) {
                board.getNotes().add(note);
            }
        });
    }

    @Transactional
    public void removeNoteEvent(Room room, String noteToRemoveUuid) {
        room.getBoards().forEach(board ->
                board.setNotes(
                        board.getNotes()
                                .stream()
                                .filter(n -> !n.getUuid().equals(noteToRemoveUuid))
                                .collect(Collectors.toList()))
        );
    }

    @Transactional
    public void addVoteNoteEvent(Room room, String noteVoteUuid, String userVotedUuid) {
        room.getBoards().forEach(board ->
                board.getNotes().forEach(note -> {
                    if (note.getUuid().equals(noteVoteUuid) && !note.getVotes().contains(userVotedUuid)) {
                        note.getVotes().add(userVotedUuid);
                    }
                })
        );
    }

    @Transactional
    public void removeVoteNoteEvent(Room room, String noteVoteUuid, String userVotedUuid) {
        room.getBoards().forEach(board ->
                board.getNotes().forEach(note -> {
                    if (note.getUuid().equals(noteVoteUuid)) {
                        note.setVotes(note.getVotes()
                                .stream()
                                .filter(v -> !v.equals(userVotedUuid))
                                .collect(Collectors.toList())
                        );
                    }
                })
        );
    }

    @Transactional
    public void moveNoteEvent(Room room, String targetBoardUuid, Note note) {
        removeNoteEvent(room, note.getUuid());
        addNoteEvent(room, targetBoardUuid, note);
    }

    @Transactional
    public void editNoteEvent(Room room, String boardUuid, Note note) {
        removeNoteEvent(room, note.getUuid());
        addNoteEvent(room, boardUuid, note);
    }

}
