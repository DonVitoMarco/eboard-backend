package pl.thewalkingcode.eboard.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.model.RoomUsers;
import pl.thewalkingcode.eboard.model.response.CreateRoomResponse;
import pl.thewalkingcode.eboard.repository.RoomRepository;
import pl.thewalkingcode.eboard.repository.RoomUsersRepository;

import java.util.Date;
import java.util.UUID;

@RestController
public class ImportController {

    private RoomRepository roomRepository;
    private RoomUsersRepository roomUsersRepository;

    @Autowired
    public ImportController(RoomRepository roomRepository, RoomUsersRepository roomUsersRepository) {
        this.roomRepository = roomRepository;
        this.roomUsersRepository = roomUsersRepository;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public CreateRoomResponse importMethod(@RequestBody Room requestRoom) throws Exception {
        requestRoom.setUuid(UUID.randomUUID().toString());
        requestRoom.setCreateDate(new Date());
        String roomUsersUuid = UUID.randomUUID().toString();
        requestRoom.setUsersUuid(roomUsersUuid);
        roomRepository.save(requestRoom);
        RoomUsers roomUsers = new RoomUsers(roomUsersUuid, requestRoom.getUuid());
        roomUsersRepository.save(roomUsers);
        return new CreateRoomResponse(requestRoom.getUuid());
    }

}
