package pl.thewalkingcode.eboard.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.model.request.CreateRoomRequest;
import pl.thewalkingcode.eboard.model.response.CreateRoomResponse;
import pl.thewalkingcode.eboard.service.RoomService;

@RestController
@RequestMapping("/api/room")
public class RoomController {

    private RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.POST)
    public CreateRoomResponse createRoom(@RequestBody CreateRoomRequest createRoomRequest) {
        Room room = roomService.createRoom(createRoomRequest);
        return new CreateRoomResponse(room.getUuid());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{roomUuid}", method = RequestMethod.GET)
    public Room getRoom(@PathVariable String roomUuid) {
        return roomService.findRoom(roomUuid);
    }

}
