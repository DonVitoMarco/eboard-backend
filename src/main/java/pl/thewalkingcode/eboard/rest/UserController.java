package pl.thewalkingcode.eboard.rest;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.thewalkingcode.eboard.model.RoomUsers;
import pl.thewalkingcode.eboard.model.User;
import pl.thewalkingcode.eboard.model.response.ResponseString;
import pl.thewalkingcode.eboard.repository.RoomUsersRepository;
import pl.thewalkingcode.eboard.stomp.WebSocketMessaging;
import pl.thewalkingcode.eboard.stomp.model.UserMessage;

import java.util.List;

@RestController
@RequestMapping("/api/room")
public class UserController {

    private RoomUsersRepository roomUsersRepository;
    private WebSocketMessaging webSocketMessaging;

    @Autowired
    public UserController(RoomUsersRepository roomUsersRepository, WebSocketMessaging webSocketMessaging) {
        this.roomUsersRepository = roomUsersRepository;
        this.webSocketMessaging = webSocketMessaging;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{roomUuid}/user/{userUuid}", method = RequestMethod.POST)
    public User addUser(@PathVariable String roomUuid, @PathVariable String userUuid) {
        RoomUsers roomUsers = roomUsersRepository.findByRoomUuid(roomUuid);
        Gson gson = new Gson();
        if (roomUsers != null) {
            User user = new User(userUuid, "anonymous");
            roomUsers.getUsers().add(user);
            roomUsersRepository.save(roomUsers);
            webSocketMessaging.sendToAllUsers(roomUuid, gson.toJson(new UserMessage("ADD", user)));
            return user;
        }
        return null;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{roomUuid}/user/{userUuid}", method = RequestMethod.DELETE)
    public User removeUser(@PathVariable String roomUuid, @PathVariable String userUuid) {
        RoomUsers roomUsers = roomUsersRepository.findByRoomUuid(roomUuid);
        Gson gson = new Gson();
        User user = null;
        if (roomUsers != null) {
            for (User u : roomUsers.getUsers()) {
                if (userUuid.equals(u.getUuid())) {
                    user = u;
                }
            }
            if (user != null) {
                roomUsers.getUsers().remove(user);
                roomUsersRepository.save(roomUsers);
                webSocketMessaging.sendToAllUsers(roomUuid, gson.toJson(new UserMessage("REMOVE", user)));
            }
        }
        return user;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{roomUuid}/user/{userUuid}", method = RequestMethod.PUT)
    public ResponseString changeUsername(@PathVariable String roomUuid, @PathVariable String userUuid, @RequestBody User user) {
        RoomUsers roomUsers = roomUsersRepository.findByRoomUuid(roomUuid);
        Gson gson = new Gson();
        for (User u : roomUsers.getUsers()) {
            if (userUuid.equals(u.getUuid())) {
                u.setUsername(user.getUsername());
            }
        }
        roomUsersRepository.save(roomUsers);
        webSocketMessaging.sendToAllUsers(roomUuid, gson.toJson(new UserMessage("CHANGE", user)));
        return new ResponseString(user.getUsername());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{roomUuid}/users", method = RequestMethod.GET)
    public List<User> getUsers(@PathVariable String roomUuid) {
        return roomUsersRepository.findByRoomUuid(roomUuid).getUsers();
    }

}
