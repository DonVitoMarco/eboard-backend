package pl.thewalkingcode.eboard.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.service.RoomService;

@RestController
@RequestMapping(path = "/export", method = RequestMethod.GET)
public class ExportController {

    private RoomService roomService;

    @Autowired
    public ExportController(RoomService roomService) {
        this.roomService = roomService;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/{uuid}", produces = "application/json")
    public Room exportMethod(@PathVariable String uuid) {
        Room room = roomService.findRoom(uuid);
        if (room == null) {
            return null;
        }
        room.setUsersUuid("");
        room.setUuid("");
        return room;
    }

}
