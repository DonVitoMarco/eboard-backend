package pl.thewalkingcode.eboard.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.thewalkingcode.eboard.model.Room;
import pl.thewalkingcode.eboard.repository.RoomRepository;
import pl.thewalkingcode.eboard.repository.RoomUsersRepository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class DbScheduler {
    private RoomRepository roomRepository;
    private RoomUsersRepository roomUsersRepository;

    @Autowired
    public DbScheduler(RoomRepository roomRepository, RoomUsersRepository roomUsersRepository) {
        this.roomRepository = roomRepository;
        this.roomUsersRepository = roomUsersRepository;
    }

    @Scheduled(cron = "0 0 12 * * ?")
    public void removeOldData() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -7);
        Date dateToCompare = calendar.getTime();

        List<Room> rooms = roomRepository.findAll();
        for (Room room : rooms) {
            if (dateToCompare.after(room.getCreateDate())) {
                roomRepository.delete(room.getUuid());
                roomUsersRepository.delete(room.getUsersUuid());
            }
        }
    }

}
